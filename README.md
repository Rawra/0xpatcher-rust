# 0xpatcher-rust

## Name
0xPatcher-rust: A binary patcher based on pattern searching with wildcards.

## Usage

Usage is pretty straightforward:
```0xpatcher-rust.exe --input _INPUT_FILE_PATH_```
the patcher will automatically create a backup file with the same name with an added _o suffix.

It will go through all available patches in the patches folder and will try to apply every matching one.

Its important to know that the patcher requires a "patches" folder in the same directory as the patcher itself with the following structure:
```
patches\my_cool_patch.json
patches\my_cool_patch2.json
```

A patch json can look like this:
```
{
  "targets": [
    "my_binary_file.exe"
  ],
  "author": "Rawra",
  "description": "Opcode fix",
  "patches": [
    {
      "pattern": "B7 ?? ?? 2A B4 ?? ??",
      "replace": "00 00 00 00 00 00 00",
      "offset": 0
    }
  ]
}
```
It may include multiple patches with varying offsets (the offset will be applied to the location where the pattern has been found; from there on out the 
replacement bytes will be added)

## Installation

Clone the project and build using ```cargo build```

## Credits

| Library     | Link                                         |
|-------------|----------------------------------------------|
| Patternscan | https://github.com/lewisclark/patternscan    |
| serde_json  | https://github.com/serde-rs/json             |
| clap        | https://github.com/clap-rs/clap              |
| serde       | https://github.com/serde-rs/serde            |
| hex         | https://github.com/KokaKiwi/rust-hex         |
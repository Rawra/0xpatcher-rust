use core::panic;
use std::{fmt};
use std::fs::{self, File, ReadDir};
use std::io::{Error, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};
use hex::FromHexError;
use patternscanner::PatternScannerBuilder;
use serde::{Deserialize, Serialize};
use clap::{arg, command, Parser};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Input file path
    #[arg(short, long)]
    input: String,

    /// Backup file path
    #[arg(short, long, default_value_t = String::from(""))]
    backup: String,
}

/*
    0xPatcher-rust: A Universal AOB Patcher

    ToDo:
    - Implement -n (patch info)
    - Implement -l (list patches)
*/

// Entrypoint
fn main() {
    let args = Args::parse();

    // Load all avilable patches
    let patches: Vec<PatchInfo> = match load_patches() {
        Ok(v) => v,
        Err(e) => {
            panic!("Unrecoverable error while trying to load patches: {}", e.to_string());
        }
    };
    println!("Loaded:\t[{}] patches", patches.len());
    
    //println!("Args.Input: {}", &args.input);
    apply_patches(&args.input,  &args.backup, &patches);
    return;
}

// Represents general info describing a patch
#[derive(Serialize, Deserialize, Clone)]
struct PatchInfo {
    targets: Vec<String>,
    author: String,
    description: String,
    patches: Vec<Patch>
}

// Represents the patch itself including its pattern, replacement bytes as hex strings and the offset
// where the offset will be applied upon finding the pattern [pattern location + offset]
#[derive(Serialize, Deserialize, Clone)]
struct Patch {
    pattern: String,
    replace: String,
    offset: u64
}

// Define our error types. These may be customized for our error handling cases.
// Now we will be able to write our own errors, defer to an underlying error
// implementation, or do something in between.
#[derive(Debug, Clone)]
struct LoadPatchesError;

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for LoadPatchesError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "error trying to load patches")
    }
}

// ToDo
fn list_patches(available_patches: &Vec<PatchInfo>) {
    println!("Showing patches...");
    for patch_info in available_patches {
        println!("Patch info [{}] with [{}] patches", patch_info.description, patch_info.patches.len());
        println!("\tTarget(s): {}", patch_info.targets.join(", "));
        println!("\tDescription: {}", patch_info.description);
        println!("\tAuthor: {}", patch_info.author);
        for patch in patch_info.patches.as_slice() {
            println!("\t\tPattern: {}", patch.pattern);
            println!("\t\tReplace: {}", patch.replace);
            println!("\t\tOffset.: {}", patch.offset);
            println!("");
        }
        println!("");
    }
}

// Will try to go over all patches that are compatible
// with the selected binary and will go on to apply
// every patch individually
#[warn(unused_labels)]
fn apply_patches(input_file_path_str: &String, backup_file_path_str: &String, available_patches: &Vec<PatchInfo>) {
    // Make sure file exists
    let input_file_path = Path::new(input_file_path_str);
    match input_file_path.exists() {
        false => {
            println!("Input file missing! [{}]", input_file_path_str);
            return;
        },
        _ => println!("Searching for available patches...")
    }

    // Open the target binary in read/write mode
    let mut file = match File::options().read(true).write(true).open(input_file_path_str) {
        Err(e) => {
            println!("Can't open input file! [{}]", e.to_string());
            return;
        },
        Ok(v) => v
    };
    println!("Target file:\t[{}]", input_file_path_str);

    // Read the file into memory
    let mut buf_file: Vec<u8> = Vec::new();
    let _buf_file_read = match file.read_to_end(&mut buf_file) {
        Err(e) => {
            println!("Error while trying to read input file into memory! [{}]: [{}]", input_file_path_str, e.to_string());
            return;
        },
        Ok(v) => v
    };

    // Iterate through all loaded patches
    '_outer: for patch_info in available_patches {
        // Make sure to ignore non-target patches
        if !patch_info.targets.contains(&input_file_path.file_name().unwrap().to_str().unwrap().to_string()) {
            continue;
        }

        // Create backup file
        let backup_path: String = match backup_file_path_str.as_str() {
            "" => input_file_path_str.clone() + "_o",
            _ => backup_file_path_str.clone()
        };

        println!("Creating backup:\t[{}]", backup_path);
        match fs::copy(input_file_path, backup_path) {
            Err(e) => println!("\tError while trying to create backup: {}", e.to_string()),
            _ => ()
        }

        // Try to apply the patch
        println!("Trying patch:\t[{}]", &patch_info.description);
        'inner: for patch in &patch_info.patches {
            println!("\tSearching pattern:\t[{}]", &patch.pattern);

            // Try to find the pattern in the file with error handling
            let locs = match PatternScannerBuilder::builder()
            .with_bytes(&buf_file)
            .with_threads(8)
            .build()
            .scan_all(&patch.pattern) {
                Err(e) => {
                    println!("\tError while trying to find pattern:\t[{}]: {}", patch.pattern, e.to_string());
                    continue;
                },
                Ok(v) => v
            };
            //println!("\tSearching pattern:\t[{}] OK", &patch.pattern);

            // Check if we even have matches at all
            if locs.len() <= 0 {
                println!("\tPattern not found:\t[{}]\n", patch.pattern);
                continue;
            }

            /*
            Generally when you have a collection of one type in Rust, and want to turn it to another type, you call .iter().map(...) on it. 
            The advantage of this method is you keep your ids as integers which is nice, have no mutable state, and don't need an extra library. 
            Also if you want a more complex transformation than just a casting, this is a very good method 
            */
            let mut locs_str: String = locs.iter().map( | &id | format!("{:#010X}, ", id)).collect();

            // remove trailing comma and space
            locs_str.pop();
            locs_str.pop();

            println!("\tPattern(s) found at:\t[{}]\n", locs_str);
            // Start from the beginning for the next operation
            match &file.rewind() {
                Err(e) => println!("\tError trying to rewind: {}", e.to_string()),
                _ => ()
            }

            // Apply the patch
            apply_patch(&input_file_path_str, &mut &file, &patch, &locs);
            continue 'inner;
        }

        // Flush writing (finish all operations)
        match &file.flush() {
            Err(e) => println!("\tError while trying to flush: {}", e.to_string()),
            _ => ()  
        }
    }
}

// Applies individual patches to binaryfile at the designated
// locations set by locs
fn apply_patch(input_file_path: &String, writer: &mut &File, patch: &Patch, locs: &Vec<usize>) {
    for &loc in locs {
        println!("\t\tApplying patch at:\t[{:#010X}h + {}d]", &loc, &patch.offset);
        let addr: u64 = loc.try_into().unwrap();

        // Set the reader position to our location + patch defined offset
        println!("\t\tSeeking from file:\t[{}]", input_file_path);
        match writer.seek(SeekFrom::Start(&addr + &patch.offset)) {
            Err(e) => {
                println!("\t\tError while trying to apply patch: {}", e.to_string());
                continue;
            },
            _ => (),
        }
        //println!("\t\tSeeking from file:\t[{}] OK", input_file_path);

        // Try to parse the replacement bytes hex string to a proper byte array
        let replace_bytes: Result<Vec<u8>, FromHexError> = hex::decode(&patch.replace);
        match replace_bytes {
            Err(e) => {
                println!("\t\tError while trying to decode replace hex string: {}", e.to_string());
                continue;
            },
            Ok(v) => {
                // Try to write to the file (still needs to be flushed)
                println!("\t\tWriting to file:\t[{}]", input_file_path);
                match writer.write(v.as_slice()) {
                    Err(e) => println!("\tError while trying to write to target file: {}", e.to_string()),
                    _ => ()
                }
                //println!("\t\tWriting to file:\t[{}] OK", input_file_path);
            }
        }
        println!("\n");
    }

}

// Loads all available patches from special directory
fn load_patches() -> Result<Vec<PatchInfo>, LoadPatchesError> {
    let mut patches: Vec<PatchInfo> = Vec::new();
    let patches_path = std::env::current_dir().unwrap().join("patches");
    let entries: Result<ReadDir, Error> = fs::read_dir(&patches_path);
    
    // Check if patches dir exists
    match &patches_path.exists() {
        false => {
            println!("Patches directory missing! [{}]", &patches_path.into_os_string().into_string().unwrap());
            return Err(LoadPatchesError);
        },
        _ => println!("Loading patches...")
    }

    // For every json patch
    for entry in entries.unwrap() {
        let patch_path: PathBuf = entry.as_ref().unwrap().path();
        let patch_path_ext: &str = match patch_path.extension() {
            None => {
                continue;
            },
            Some(v) => v.to_str().unwrap()
        };

        if !patch_path_ext.eq("json") {
            continue;
        }

        // Try to load the patch
        let patch_info = match load_patch(&patch_path) {
            Ok(patch) => patch,
            Err(e) => {
                println!("Failed to load patch: {}", e.to_string());
                continue;
            }
        };
        
        patches.push(patch_info);
    }

    return Ok(patches);
}

// Loads individual patches
fn load_patch(patch_patch: &PathBuf) -> Result<PatchInfo, serde_json::Error> {
    let patch_path_str: String =  patch_patch.as_os_str().to_str().unwrap().to_string();
    //println!("Loading patch [{}]", &patch_path_str);

    let patch_str: Result<String, Error> = fs::read_to_string(patch_path_str);
    let mut patch_info: Result<PatchInfo, serde_json::Error> = serde_json::from_str(&patch_str.unwrap());

    // Check json parsing result
    match &mut patch_info {
        Ok(v) => {
            //println!("Loaded patch for [{}]", v.targets.join(", "));

            // Fix replacement hex-string encoding
            for patch in v.patches.iter_mut() {
                patch.replace = patch.replace.replace(" ", "");
            }
        }
        Err(e) => println!("Error parsing json: {}", e.to_string())
    }

    return patch_info;
}